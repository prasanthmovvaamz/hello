package main

import (
	"fmt"
	"os"

	"gitlab.com/jaime/hello/internal/hello"
)

func main() {
	name := ""
	if len(os.Args) > 1 {
		name = os.Args[1]
	}

	fmt.Println(hello.Greet(name))
}
